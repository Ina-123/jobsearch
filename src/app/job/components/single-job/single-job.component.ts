import { Component, OnInit } from '@angular/core';
import { Observable, catchError, delay, switchMap, throwError } from 'rxjs';
import { Job } from '../../models/job.models';
import { JobService } from '../../services/job.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-single-job',
  templateUrl: './single-job.component.html',
  styleUrl: './single-job.component.css'
})
export class SingleJobComponent implements OnInit{
  job$! : Observable<Job>;
  errorMessage! : string | null;

  constructor(
    private activateRoute : ActivatedRoute,
    private router : Router,
    private jobService : JobService){}

  ngOnInit(): void {
    this.errorMessage = null;
    this.job$ = this.activateRoute.params.pipe(
      delay(1000),
      switchMap(params => this.jobService.getJobById(+params['id'])),
      catchError((error : string) =>{
        this.errorMessage = error;
        return throwError('error');
      })
    )
  }

  onGoBack() : void{
    this.router.navigateByUrl('/');
  }
}
