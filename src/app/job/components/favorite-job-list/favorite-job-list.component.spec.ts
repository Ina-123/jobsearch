import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteJobListComponent } from './favourite-job-list.component';

describe('FavouriteJobListComponent', () => {
  let component: FavouriteJobListComponent;
  let fixture: ComponentFixture<FavouriteJobListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FavouriteJobListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FavouriteJobListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
